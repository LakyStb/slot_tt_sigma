package;

import flixel.FlxGame;
import game.Game;

/**
 * ...
 * @author NickKarhalsky
 */
class Main extends FlxGame
{

	public function new() 
	{
		super(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT, Game);
	}

}
