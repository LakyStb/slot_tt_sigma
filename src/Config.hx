package;

/**
 * ...
 * @author NickKarhalsky
 */
class Config
{
	
	inline public static var SCREEN_WIDTH:Int = 800;
	inline public static var SCREEN_HEIGHT:Int = 600;
	
	
	inline public static var NUM_REELS:Int = 3;
	inline public static var NUM_SYMBOLS:Int = 10;
	
	inline public static var REEL_TURN_AMOUNT:Int = 3;
	
}