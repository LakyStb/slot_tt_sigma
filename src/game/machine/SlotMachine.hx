package game.machine;

import flixel.group.FlxSpriteGroup;
import game.machine.component.ButtonSpin;
import game.machine.component.Reel;

/**
 * ...
 * @author NickKarhalsky
 */
class SlotMachine extends FlxSpriteGroup
{
	
	private var _reelsContainer:FlxSpriteGroup;
	private var _reelsList:Array<Reel>;
	
	private var _btnSpin:ButtonSpin;
	
	private var _reelsRolsCounter:Int = 0;
	

	public function new(X:Float=0, Y:Float=0, MaxSize:Int=0) 
	{
		super(X, Y, MaxSize);
		
		createReels();
		createButtonSpin();
	}
	
	
	private function createReels():Void
	{
		_reelsList = [];
		
		_reelsContainer = new FlxSpriteGroup();
		add(_reelsContainer);
		
		var reel:Reel;
		for (i in 0...Config.NUM_REELS) 
		{
			reel = new Reel();
			reel.setPosition(reel.width * i);
			_reelsList.push(reel);
			
			_reelsContainer.add(reel);
		}
	}
	
	private function createButtonSpin():Void
	{
		_btnSpin = new ButtonSpin(0, 0, "", onButtonSpinClick);
		_btnSpin.setPosition(
			_reelsContainer.x + _reelsContainer.width + _btnSpin._halfWidth,
			_reelsContainer.y + _reelsContainer.height - _btnSpin.height
		);
		
		add(_btnSpin);
	}
	
	
	private function onButtonSpinClick():Void
	{
		deactivateBtnSpin();
		
		for (reel in _reelsList) 
		{
			reel.spin(Config.REEL_TURN_AMOUNT + _reelsRolsCounter, onSpinComplete);
			_reelsRolsCounter++;
		}
	}
	
	private function onSpinComplete():Void
	{
		_reelsRolsCounter--;
		if (_reelsRolsCounter <= 0) 
		{
			activateBtnSpin();
		}
	}
	
	
	private function activateBtnSpin():Void { _btnSpin.active = true; }
	private function deactivateBtnSpin():Void { _btnSpin.active = false; }
	
	
	override public function destroy():Void 
	{
		_btnSpin.destroy();
		_btnSpin = null;
		
		_reelsContainer.destroy();
		_reelsContainer = null;
		
		_reelsList = null;
		
		super.destroy();
	}
}