package game.machine.component;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.util.FlxColor;
import flixel.util.FlxRandom;
import manager.AssetsManager;
import motion.Actuate;
import motion.easing.Linear;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author NickKarhalsky
 */
class Reel extends FlxSpriteGroup
{
	
	private var _foreground			:FlxSprite;
	
	private var _symbolsList		:Array<FlxSprite>;
	private var _renderedSymbols	:FlxSprite;
	private var _symbolsStrip		:FlxSprite;
	
	
	private var _symbolWidth:Int;
	private var _symbolHeight:Int;
	
	private var _renderRectWidth:Int;
	private var _renderRectHeight:Int;
	private var _renderOffsetY:Int;
	
	private var _spinCompleteCallback:Void -> Void;
	
	
	public function new(X:Float=0, Y:Float=0, MaxSize:Int=0) 
	{
		super(X, Y, MaxSize);
		
		_renderedSymbols = new FlxSprite();
		add(_renderedSymbols);
		
		_foreground = new FlxSprite(0, 0, AssetsManager.getReelForeground());
		add(_foreground);
		
		createSymbols();
		prepareRenderParams();
		renderSymbols();
	}
	
	
	private function createSymbols():Void
	{
		_symbolsList = AssetsManager.getReelSymbols();
		
		var availableSymbolsAmount:Int = _symbolsList.length - 1;
		var symbol:FlxSprite = _symbolsList[0];
		
		_symbolWidth = Std.int(symbol.width);
		_symbolHeight = Std.int(symbol.height);
		
		_symbolsStrip = new FlxSprite();
		_symbolsStrip.makeGraphic(_symbolWidth, _symbolHeight * Config.NUM_SYMBOLS, FlxColor.TRANSPARENT);
		
		for (i in 0...Config.NUM_SYMBOLS) 
		{
			symbol = _symbolsList[FlxRandom.intRanged(0, availableSymbolsAmount)];
			symbol.setPosition(0, symbol.height * i);
			
			_symbolsStrip.stamp(symbol, Std.int(symbol.x), Std.int(symbol.y));
		}
		
		_symbolsStrip.setPosition((_foreground.width - _symbolsStrip.width) * 0.5, 0);
	}
	
	
	//===================================> RENDERING >>>
	private function prepareRenderParams():Void
	{
		// constant value it's magic number chosen empirically - borders of foreground
		_renderRectWidth = Std.int(_foreground.width * 0.85);
		_renderRectHeight = Std.int(_foreground.height * 0.95);
		_renderOffsetY = Std.int(_symbolsStrip.height - (_foreground.height * 0.85));
	}
	
	private function renderSymbols():Void
	{
		var offset:Float = _renderOffsetY - _symbolsStrip.y;
		var newSpriteRegion:Rectangle = new Rectangle(0, offset, _renderRectWidth, _renderRectHeight);
		var sourceBitmapData:BitmapData = _symbolsStrip.getFlxFrameBitmapData();
		var bitmapRegion:BitmapData = new BitmapData(_renderRectWidth, _renderRectHeight, true, 0x00FFFFFF);
		
		bitmapRegion.copyPixels(sourceBitmapData, newSpriteRegion, new Point(_symbolsStrip.x, 20), null, null, true);
		_renderedSymbols.loadGraphic(bitmapRegion, false, _renderRectWidth, _renderRectHeight, true);
	}
	//===================================> RENDERING <<<
	
	
	//===================================> SPIN HANDLER >>>
	public function spin(repeatAmount:Int, callback:Void -> Void):Void
	{
		_spinCompleteCallback = callback;
		_symbolsStrip.y = 0;
		
		Actuate.tween(_symbolsStrip, 0.3, { y: _renderOffsetY } ).repeat(repeatAmount).ease(Linear.easeNone).onUpdate(updateSymbolsStrip).onComplete(onCompleteSpin);
	}
	
	private function updateSymbolsStrip():Void
	{
		renderSymbols();
	}
	
	private function onCompleteSpin():Void
	{
		var targetY:Int = FlxRandom.intRanged(0, Config.NUM_SYMBOLS - 3) * _symbolHeight;
		_symbolsStrip.y = targetY;
		
		renderSymbols();
		
		if (_spinCompleteCallback != null) 
		{
			_spinCompleteCallback();
		}
	}
	//===================================> SPIN HANDLER <<<
	
	
	override public function destroy():Void 
	{
		_foreground.destroy();
		_foreground = null;
		
		_renderedSymbols.destroy();
		_renderedSymbols = null;
		
		_symbolsStrip.destroy();
		_symbolsStrip = null;
		
		_symbolsList = null;
		
		super.destroy();
	}
}