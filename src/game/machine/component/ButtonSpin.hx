package game.machine.component;

import flixel.ui.FlxButton;
import manager.AssetsManager;

/**
 * ...
 * @author NickKarhalsky
 */
class ButtonSpin extends FlxButton
{

	public function new(X:Float=0, Y:Float=0, ?Text:String, ?OnClick:Void->Void) 
	{
		super(X, Y, Text, OnClick);
		
		loadGraphic(AssetsManager.getButtonSpin(), false, 128, 64);
	}
	
}