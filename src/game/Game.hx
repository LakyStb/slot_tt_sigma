package game;

import flixel.FlxG;
import flixel.FlxState;
import flixel.util.FlxColor;
import game.machine.SlotMachine;

/**
 * ...
 * @author NickKarhalsky
 */
class Game extends FlxState
{
	
	private var _slotMachine:SlotMachine;
	
	public function new():Void
	{
		super();
	}
	
	override public function create():Void 
	{
		super.create();
		
		bgColor = FlxColor.OLIVE;
		
		_slotMachine = new SlotMachine();
		_slotMachine.setPosition(
			(FlxG.width - _slotMachine.width) * 0.5, 
			(FlxG.height - _slotMachine.height) * 0.5
		);
		add(_slotMachine);
	}
	
	override public function destroy():Void 
	{
		_slotMachine.destroy();
		_slotMachine = null;
		
		super.destroy();
	}
}