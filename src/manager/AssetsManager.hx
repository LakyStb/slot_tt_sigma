package manager;
import flixel.FlxSprite;
import haxe.ds.StringMap;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.DisplayObject;

/**
 * ...
 * @author NickKarhalsky
 */
class AssetsManager
{
	private inline static var BTN_SPIN_PATH			:String = "img/btn_spin.png";
	private inline static var REEL_FG_PATH			:String = "img/reel_fg.png";	// foreground of reel
	
	private inline static var REEL_SYMBOLS_PREFIX	:String = "img/item_";
	
	
	private static var _symbolsPostfixList			:Array<String> = [];
	
	
	public inline static function getButtonSpin():BitmapData
	{
		return Assets.getBitmapData(BTN_SPIN_PATH);
	}
	
	public inline static function getReelForeground():BitmapData
	{
		return Assets.getBitmapData(REEL_FG_PATH);
	}
	
	/**
	 * @return map with key value pair: image name - image sprite
	 */
	public inline static function getReelSymbols():Array<FlxSprite>
	{
		if (_symbolsPostfixList.length == 0)
		{
			// parsing - getting all images with prefix item
			var imgPathList:Array<String> = Assets.list(AssetType.IMAGE);
			var imgName:String;
			
			for (path in imgPathList) 
			{
				if (path.indexOf(REEL_SYMBOLS_PREFIX) != -1) 
				{
					imgName = path.split("_")[1]; // cutoff prefix
					imgName = imgName.split(".")[0]; // cutoff extension
					
					_symbolsPostfixList.push(imgName);
				}
			}
		}
		
		var reelSymbols:Array<FlxSprite> = [];
		
		for (imgName in _symbolsPostfixList) 
		{
			reelSymbols.push(new FlxSprite(0, 0, REEL_SYMBOLS_PREFIX + imgName + ".png"));
		}
		
		return reelSymbols;
	}
}